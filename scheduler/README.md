# Scheduler

**Python3 needed**

## Setting up your own virtual environment

Run `make virtualenv` to create a virtual environment.
then activate it with `source .venv/bin/activate`.

## Install the project in develop mode

Run `make install` to install the project in develop mode.

## Run the tests to ensure everything is working

Run `make test` to run the tests.

## Build the docs locally

Run `make docs` to build the docs.

Ensure your new changes are documented.

## Makefile utilities

This project comes with a `Makefile` that contains a number of useful utility.

```bash
❯ make
Usage: make <target>
Targets:
help:             ## Show the help.
install:          ## Install the project in dev mode.
fmt:              ## Format code using black & isort.
lint:             ## Run pep8, black, mypy linters.
test: lint        ## Run tests and generate coverage report.
watch:            ## Run tests on every change.
clean:            ## Clean unused files.
virtualenv:       ## Create a virtual environment.
release:          ## Create a new tag for release.
docs:             ## Build the documentation.
switch-to-poetry: ## Switch to poetry package manager.
init:             ## Initialize the project based on an application template.
```

## Celery

Start celery beat

```bash
❯ celery -A scheduler.worker beat -l info --scheduler celery_sqlalchemy_scheduler.schedulers:DatabaseScheduler
```

Start celery worker

```console
❯ celery -A scheduler.worker worker -l info
```

## Start application

Setup database

```console
$ scheduler create-db
```

Start Celery

```console
$ scheduler run
```

## Add dummy engine

```console
curl -X POST --header "Content-Type: application/json" http://127.0.0.1:5000/api/v1/jobs --data '{"repo_url": "https://scheduler:glpat-MsDsrHMH-k3-DzEfNRgk@gitlab.digitallearning.gmbh/polaris/engines/dummy-engine.git", "name": "Dummy job", "crontab": "*/1 * * * *"}'
```

## API Documentation

A Swagger documentation is available under `/swagger-ui/`.

## Commands

```bash
❯ scheduler --help
Commands
create-db    Creates database
drop-db      Cleans database
routes       Show the routes for the app.
run          Run a development server.
shell        Runs a shell in the app context.
```
