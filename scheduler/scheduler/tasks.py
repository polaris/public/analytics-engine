import os
import shutil
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen

import git
import requests
from celery import shared_task
from dynaconf import settings

from scheduler.ext.database import db
from scheduler.models import Job

ANALYTICS_BACKEND = settings.get("ANALYTICS_BACKEND_URL", "")
RIGHTS_ENGINE_BACKEND = settings.get("RIGHTS_ENGINE_BACKEND_URL", "")

def append_job_log(result_token, job_id, job_output, succeeded=True):
    url = f"{ANALYTICS_BACKEND}/api/v1/jobs/{job_id}/log"
    response = requests.post(
        url,
        json={"result_token": result_token, "log": job_output, "succeeded": succeeded},
    )
    if not response.ok:
        print("Failed to send job log to backend - returned status code " + str(response.status_code))


def run_results_retention(analytics_token):
    url = f"{RIGHTS_ENGINE_BACKEND}/api/v1/provider/results-retention"
    response = requests.post(url, headers={"Authorization": f"Basic {analytics_token}"})
    if not response.ok:
        raise RuntimeError("Failed to execute results retention, rights engine request returned status code " + str(response.status_code))


@shared_task(name="celery_tasks.run_analysis")
def run_analysis(repo_url, job_id, result_token, analytics_token):
    directory = f"job_{job_id}_repo"
    try:
        git.Repo.clone_from(repo_url, directory)
    except Exception as e:
        message = f"Repo {repo_url} inaccessible ({e})."
        append_job_log(result_token, job_id, message, False)
        if os.path.exists(directory):
            shutil.rmtree(directory)
        return f"Job with id {job_id} failed - {message}"

    if not Path(f"{directory}/main.py").is_file():
        append_job_log(result_token, job_id, "main.py doesn't exist", False)
        return f"Job with id {job_id} failed. main.py doesn't exist"

    cmd = f"python3 {directory}/main.py {analytics_token} {RIGHTS_ENGINE_BACKEND}"
    p = Popen(
        cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True
    )
    stdout, stderr = p.communicate()
    print("Job log: ", stdout.decode("utf-8"))
    append_job_log(result_token, job_id, stdout.decode("utf-8"))
    shutil.rmtree(directory)

    try:
        run_results_retention(analytics_token)
    except Exception as e:
        message = f"failed while running results retention: " + str(e)
        append_job_log(result_token, job_id, message)
        return f"Job with id {job_id} {message}"
    return f"Job with id {job_id} succeeded."

