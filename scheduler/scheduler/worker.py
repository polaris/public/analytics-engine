from celery import current_app as current_celery_app
from dynaconf import settings

from .tasks import *

celery = current_celery_app.conf.update(
    beat_dburi=settings.get("SQLALCHEMY_DATABASE_URI", None),
    broker_url=settings.get("CELERY_BROKER_URL", None),
    result_backend=settings.get("CELERY_RESULT_BACKEND", None),
)


#current_celery_app.conf.beat_schedule = {
    #"process_round": {
        #"task": "celery_tasks.check_jobs",
        #"schedule": crontab(minute="*/1"),
    #},
#}
