from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from celery_sqlalchemy_scheduler import (CrontabSchedule, DatabaseScheduler,
                                         IntervalSchedule, PeriodicTask,
                                         PeriodicTaskChanged, SessionManager,
                                         SolarSchedule)
from dynaconf import FlaskDynaconf
from flask import Flask
from flask_apispec.extension import FlaskApiSpec
from flask_celeryext import FlaskCeleryExt
from prometheus_flask_exporter import PrometheusMetrics

from scheduler.ext.restapi.resources import (JobDisableResource,
                                             JobEnableResource,
                                             JobItemResource,
                                             JobRemoveResource, JobResource)

from .celery_utils import make_celery

ext_celery = FlaskCeleryExt(create_celery_app=make_celery)


def create_app(**config):
    app = Flask(__name__)
    FlaskDynaconf(app)  # config managed by Dynaconf
    app.config.load_extensions("EXTENSIONS")  # Load extensions from settings.toml
    app.config.update(config)  # Override with passed config

    app.config.update(
        {
            "APISPEC_SPEC": APISpec(
                title="Scheduler REST-API",
                openapi_version="3.0.3",
                version="v1",
                plugins=[MarshmallowPlugin()],
            ),
            "APISPEC_SWAGGER_URL": "/swagger/",
        }
    )
    docs = FlaskApiSpec(app)

    # Job control REST API is disabled in favor of YAML files
    # docs.register(JobResource, blueprint="restapi")
    # docs.register(JobItemResource, blueprint="restapi")
    # docs.register(JobEnableResource, blueprint="restapi")
    # docs.register(JobDisableResource, blueprint="restapi")
    # docs.register(JobRemoveResource, blueprint="restapi")

    ext_celery.init_app(app)
    return app


def create_app_wsgi():
    # workaround for Flask issue
    # that doesn't allow **config
    # to be passed to create_app
    # https://github.com/pallets/flask/issues/4170
    app = create_app()
    metrics = PrometheusMetrics(app, path="/metrics")
    metrics.info("app_info", "Analytics Scheduler", version="0.1.0")
    return app
