import click
from celery_sqlalchemy_scheduler import (CrontabSchedule, IntervalSchedule,
                                         PeriodicTask, PeriodicTaskChanged,
                                         SolarSchedule)
from sqlalchemy.ext.declarative import declarative_base

from scheduler.ext.database import db, engine
from scheduler.ext.restapi.resources import (create_job, parse_crontab,
                                             update_job)
from scheduler.ext.yml_config_reader import YmlConfigReader
from scheduler.models import Job


def create_db():
    db.create_all()


def drop_db():
    """Cleans database"""
    db.drop_all()

    # TODO is there a better way to delete these tables
    Base = declarative_base(bind=engine)
    to_deletes = [
        CrontabSchedule.__table__,
        IntervalSchedule.__table__,
        PeriodicTask.__table__,
        PeriodicTaskChanged.__table__,
        SolarSchedule.__table__,
    ]
    Base.metadata.drop_all(bind=engine, tables=to_deletes)


def populate_db():
    """Populate db with sample data"""
    data = []
    db.session.bulk_save_objects(data)
    db.session.commit()
def init_app(app):
    # Add multiple commands in a bulk
    for command in [create_db, drop_db, populate_db]:
        app.cli.add_command(app.cli.command()(command))

    @app.cli.command()
    def read_configs():
        """
        Read all YAML files stored in the configurations directory and create corresponding jobs.
        Jobs not found in the configurations will be deleted.
        """
        reader = YmlConfigReader()
        try:
            yaml_configs = reader.read_configs()
            
            # Create a set of job names from the YAML configurations
            config_job_names = {config["engine_name"] for config in yaml_configs}
            
            # Check for jobs in the database that are not in the configurations
            all_jobs = Job.query.all()
            for job in all_jobs:
                if job.name not in config_job_names:
                    # Delete jobs not present in the configurations
                    print(f"Deleting job {job.name} as it is no longer in the configurations.")
                    delete_job(job.name)  # Assuming a `delete_job` function exists

            # Create or update jobs based on the YAML configurations
            for config in yaml_configs:
                existing_job = Job.query.filter(Job.name == config["engine_name"]).first()
                if existing_job:
                    print(f"Updating job {config['engine_name']}.")
                    update_job(
                        config["engine_name"],
                        config["repo_url"],
                        config["crontab"],
                        config["analytics_token"],
                    )
                else:
                    new_job_id = create_job(
                        config["engine_name"],
                        config["repo_url"],
                        config["crontab"],
                        config["analytics_token"],
                    )
                    print(f"New Job {new_job_id} created.")
        except ValueError as e:
            print(e)