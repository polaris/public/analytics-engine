from marshmallow import Schema, fields


class JobLogSchema(Schema):
    log = fields.Str()
    status = fields.Str()
    created_at = fields.Str()


class JobSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    repo_url = fields.Str()
    enabled = fields.Bool()
    crontab = fields.Str()
    last_run_at = fields.Str()
    total_run_count = fields.Int()
    logs = fields.List(fields.Nested(JobLogSchema))


class JobsSchema(Schema):
    jobs = fields.List(fields.Nested(JobSchema))


class CreateJobSchema(Schema):
    crontab = fields.Str(required=True)
    name = fields.Str(required=True)
    repo_url = fields.Str(required=True)
    analytics_token = fields.Str(required=True)


class MessageSchema(Schema):
    message = fields.Str(default="Success")


class AddJobLogSchema(Schema):
    log = fields.Str(required=True)
    result_token = fields.Str(required=True)
    succeeded = fields.Bool(required=True)
