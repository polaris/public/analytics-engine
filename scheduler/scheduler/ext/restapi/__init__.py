from flask import Blueprint
from flask_restful import Api

from .resources import (JobDisableResource, JobEnableResource, JobItemResource,
                        JobOutputResource, JobRemoveResource, JobResource)

bp = Blueprint("restapi", __name__, url_prefix="/api/v1")
api = Api(bp)


def init_app(app):
    api.add_resource(JobResource, "/jobs")
    api.add_resource(JobItemResource, "/jobs/<job_id>")
    api.add_resource(JobEnableResource, "/jobs/<job_id>/enable")
    api.add_resource(JobDisableResource, "/jobs/<job_id>/disable")
    api.add_resource(JobRemoveResource, "/jobs/<job_id>/remove")
    api.add_resource(JobOutputResource, "/jobs/<job_id>/log")
    app.register_blueprint(bp)
