import json
import secrets
from datetime import datetime

from celery_sqlalchemy_scheduler.models import (CrontabSchedule, PeriodicTask,
                                                PeriodicTaskChanged)
from celery_sqlalchemy_scheduler.session import SessionManager
from dynaconf import settings
from flask import abort
from flask_apispec import doc, marshal_with, use_kwargs
from flask_apispec.views import MethodResource
from flask_restful import Resource

from scheduler.ext.database import db, scheduler_db
from scheduler.ext.restapi.schemas import (AddJobLogSchema, CreateJobSchema,
                                           JobSchema, JobsSchema,
                                           MessageSchema)
from scheduler.models import Job, JobLog, JobLogStatusEnum


def parse_crontab(crontab):
    parts = crontab.split(" ")
    if len(parts) != 5:
        raise ValueError
    return parts


def get_job_with_task(job_id):
    job = Job.query.filter_by(id=job_id).first()

    if job is None:
        return abort(400, "Job not found.")

    periodic_task = scheduler_db.query(PeriodicTask).filter_by(id=job.task_id).first()

    if periodic_task is None:
        return abort(400, "Task not found.")

    response = job.to_dict()

    response["last_run_at"] = (
        periodic_task.last_run_at.strftime("%Y-%m-%d %H:%M:%S")
        if periodic_task.last_run_at is not None
        else "not yet executed"
    )
    response["enabled"] = periodic_task.enabled
    response["total_run_count"] = periodic_task.total_run_count

    return response


def get_crontab_schedule(crontab):
    """
    Returns existing crontab scheduler for given crontab or creates a new one.
    """
    schedule = (
        scheduler_db.query(CrontabSchedule)
        .filter_by(
            minute=crontab[0],
            hour=crontab[1],
            day_of_week=crontab[4],
            day_of_month=crontab[2],
            month_of_year=crontab[3],
        )
        .first()
    )
    if not schedule:
        schedule = CrontabSchedule(
            minute=crontab[0],
            hour=crontab[1],
            day_of_week=crontab[4],
            day_of_month=crontab[2],
            month_of_year=crontab[3],
            timezone="UTC",
        )
        scheduler_db.add(schedule)
        scheduler_db.commit()
    return schedule


def update_job(name, repo_url, crontab, analytics_token):
    """
    Update existing job (including perdiodic task) in case repo_url, crontab or analytics_token changed.
    """
    old_job = Job.query.filter(Job.name == name).first()

    if old_job is None:
        print(f"Couldn't find job with name {name}.")
        return

    schedule = get_crontab_schedule(crontab)

    periodic_task = scheduler_db.query(PeriodicTask).get(old_job.task_id)

    if periodic_task is None:
        print(f"Couldn't find periodic task for {old_job.id}.")
        return

    if schedule != periodic_task.crontab:
        # schedule changed -> update schedule in periodic task
        periodic_task.crontab = schedule
        scheduler_db.commit()
        print(f"Schedule updated in periodic task {periodic_task.task_name}")

        periodic_task_change = scheduler_db.query(PeriodicTaskChanged).first()

        if periodic_task_change is None:
            print(f"Couldn't find periodic task change.")
            return
        periodic_task_change.last_update = datetime.now()
        scheduler_db.commit()

    # Update repo_url and analytics_token
    old_kwargs = json.loads(periodic_task.kwargs)

    if old_kwargs["repo_url"] != repo_url:
        print("Repo url changed.")
        old_kwargs["repo_url"] = repo_url
        old_job.repo_url = repo_url
        old_job.analtyics_token = analytics_token
        db.session.commit()

        periodic_task.kwargs = json.dumps(old_kwargs)
        scheduler_db.commit()

    if old_kwargs["analytics_token"] != analytics_token:
        print("Analytics token changed.")
        old_kwargs["analytics_token"] = analytics_token
        old_job.repo_url = repo_url
        old_job.analtyics_token = analytics_token
        db.session.commit()

        periodic_task.kwargs = json.dumps(old_kwargs)
        scheduler_db.commit()

def create_job(name, repo_url, crontab, analytics_token):
    """
    Create a new job along with a periodic task.
    """
    print(name, crontab, repo_url)

    result_token = secrets.token_hex(32)

    new_job = Job(
        name=name,
        repo_url=repo_url,
        crontab=" ".join(crontab),
        result_token=result_token,
        analytics_token=analytics_token,
    )
    db.session.add(new_job)
    db.session.commit()

    schedule = get_crontab_schedule(crontab)

    periodic_task = PeriodicTask(
        crontab=schedule,
        name=name,
        task="celery_tasks.run_analysis",
        kwargs=json.dumps(
            {
                "repo_url": repo_url,
                "job_id": new_job.id,
                "result_token": result_token,
                "analytics_token": analytics_token,
            }
        ),
    )
    scheduler_db.add(periodic_task)
    scheduler_db.commit()

    new_job.task_id = periodic_task.id
    db.session.commit()

    return new_job.id

def delete_job(job_name):
    """
    Deletes a job and its associated periodic task by job name.

    Args:
        job_name (str): The name of the job to be deleted.

    Returns:
        str: Confirmation message of the deletion or an error message if the job/task is not found.
    """
    # Find the job by name
    job = Job.query.filter_by(name=job_name).first()

    if not job:
        print(f"Job with name '{job_name}' not found.")
        return f"Job with name '{job_name}' not found."

    # Find the associated periodic task
    periodic_task = scheduler_db.query(PeriodicTask).filter_by(id=job.task_id).first()

    if periodic_task:
        # Delete the periodic task from the scheduler database
        scheduler_db.delete(periodic_task)
        scheduler_db.commit()
        print(f"Periodic task for job '{job_name}' deleted.")

    # Delete the job from the main database
    db.session.delete(job)
    db.session.commit()
    print(f"Job '{job_name}' deleted.")

    return f"Job '{job_name}' and associated periodic task deleted."


class JobResource(MethodResource, Resource):
    """
    RESTful API Resource for managing jobs.

    Provides methods to list all jobs and create a new job.
    """

    @doc(description="Returns list of all registered jobs.", tags=["Job List"])
    @marshal_with(JobsSchema, code=200)
    def get(self):
        """
        GET endpoint to retrieve all jobs.

        Returns:
            dict: A dictionary containing a list of all jobs with their details.
        """
        jobs = Job.query.all()
        return {"jobs": [get_job_with_task(job.id) for job in jobs]}

    @doc(description="Creates a new job.", tags=["Create Job"])
    @use_kwargs(CreateJobSchema)
    @marshal_with(JobSchema, code=200)
    @marshal_with(MessageSchema, code=400)
    def post(self, **kwargs):
        """
        POST endpoint to create a new job.

        Args:
            kwargs: Job details including name, repo_url, crontab, and analytics_token.

        Returns:
            dict: Created job details or an error message.
        """
        try:
            # Parse and validate the crontab string
            crontab = parse_crontab(kwargs["crontab"])
        except ValueError:
            return {"message": "Invalid crontab."}, 400

        # Check if a job with the same name already exists
        if Job.query.filter(Job.name == kwargs["name"]).first() is not None:
            return {"message": "Job with provided name already exists."}, 400

        # Create the job
        new_job_id = create_job(
            kwargs["name"], kwargs["repo_url"], crontab, kwargs["analytics_token"]
        )

        return get_job_with_task(new_job_id), 200


class JobItemResource(MethodResource, Resource):
    @doc(description="Returns job details.", tags=["Job Details"])
    @marshal_with(JobSchema, code=200)
    def get(self, job_id):

        return get_job_with_task(job_id), 200


class JobEnableResource(MethodResource, Resource):
    @doc(description="Enable job.", tags=["Enable job"])
    @marshal_with(JobSchema, code=200)
    @marshal_with(MessageSchema, code=400)
    def get(self, job_id):
        job = Job.query.filter_by(id=job_id).first()

        if job is None:
            return {"message": "Job not found"}, 404

        periodic_task = (
            scheduler_db.query(PeriodicTask).filter_by(id=job.task_id).first()
        )
        periodic_task.enabled = True
        scheduler_db.add(periodic_task)
        scheduler_db.commit()

        return get_job_with_task(job_id), 200


class JobDisableResource(MethodResource, Resource):
    @doc(description="Disable job.", tags=["Disable job"])
    @marshal_with(JobSchema, code=200)
    @marshal_with(MessageSchema, code=400)
    def get(self, job_id):
        job = Job.query.filter_by(id=job_id).first()

        if job is None:
            return {"message": "Job not found"}, 404

        periodic_task = (
            scheduler_db.query(PeriodicTask).filter_by(id=job.task_id).first()
        )
        periodic_task.enabled = False
        scheduler_db.add(periodic_task)
        scheduler_db.commit()
        return get_job_with_task(job_id), 200


class JobRemoveResource(MethodResource, Resource):
    @doc(description="Remove job.", tags=["Remove job"])
    @marshal_with(MessageSchema, code=200)
    @marshal_with(MessageSchema, code=400)
    def get(self, job_id):
        job = Job.query.get(job_id)

        if job is None:
            return {"message": "Job not found"}, 404

        periodic_task = scheduler_db.query(PeriodicTask).get(job.task_id)

        if periodic_task is not None:
            scheduler_db.delete(periodic_task)
            scheduler_db.commit()

        db.session.delete(job)
        db.session.commit()

        return {"message": "Job removed"}, 200


class JobOutputResource(MethodResource, Resource):
    @doc(description="Append job log.", tags=["Add Job Log"])
    @use_kwargs(AddJobLogSchema)
    @marshal_with(MessageSchema, code=200)
    @marshal_with(MessageSchema, code=400)
    def post(self, job_id, **kwargs):
        log = kwargs.get("log", "")[:10000]
        result_token = kwargs["result_token"]
        succeeded = kwargs["succeeded"]

        job = Job.query.get(job_id)
        if job is None:
            return {"message": "Job not found"}, 404

        if job.result_token != result_token:
            return {"message": "Invalid token"}, 400

        job_status = (
            JobLogStatusEnum.successful if succeeded else JobLogStatusEnum.failure
        )
        job_log = JobLog(log=log, job_id=job_id, status=job_status)
        db.session.add(job_log)
        db.session.commit()
        job.logs.append(job_log)
        db.session.commit()

        # Keep 10 most recent job logs
        if len(job.logs) > 10:
            outdated_logs = (
                JobLog.query.filter_by(job_id=job_id)
                .order_by(JobLog.created_at.desc())
                .offset(10)
            )
            for log in outdated_logs:
                db.session.delete(log)
            db.session.commit()

        return {"message": "Job log updated"}, 200
