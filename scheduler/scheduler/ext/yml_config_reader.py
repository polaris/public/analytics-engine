import glob
from pathlib import Path

import yaml


class YmlConfigReader:
    """
    Analytics engines are described as YAML files in 'configuration' directory. 
    This class provides helpers to validate and parse these files.
    """

    directory = "configuration"

    def read_configs(self):
        """
        Read all YAML files in the 'configuration' directory.
        """
        config_files = []
        file_paths = (
            p.resolve()
            for p in Path(self.directory).glob("*")
            if p.suffix in {".yml", ".yaml"}
        )

        for p in file_paths:
            with p.open() as f:
                config_files.append(
                    {"file_name": p.name, "config": yaml.safe_load(f.read())}
                )

        print(f"Found {len(config_files)} configuration file(s)")

        validated_configs = self.validate_config_files(config_files)
        return validated_configs

    def validate_config_files(self, config_files):
        """
        We expect the YAML files to follow a certain form, which requires validating each analytics engine YAML configuration.
        """
        validated_configs = []
        for config_file in config_files:
            for engine_name in config_file["config"].keys():
                engine_config = self.validate_config(
                    config_file["config"][engine_name],
                    config_file["file_name"],
                    engine_name,
                )
                validated_configs.append(engine_config)

        return validated_configs

    def validate_config(self, config, file_name, engine_name):
        """
        Validates a single analytics engine YAML configuration and raises a ValueError, in case a field is invalid or missing.
        """
        repo_url = config.get("repo")
        crontab = config.get("crontab", "").split(" ")
        analytics_token = config.get("analytics_token")

        if repo_url is None:
            raise ValueError(
                f"Missing repo_url for engine '{engine_name}' in file '{file_name}'."
            )

        if len(crontab) != 5:
            raise ValueError(
                f"Invalid crontab {config.get('run', '')} for engine '{engine_name}' in file '{file_name}'."
            )

        if analytics_token is None:
            raise ValueError(
                f"Missing analytics token for engine '{engine_name}' in file '{file_name}'."
            )

        return {
            "engine_name": engine_name,
            "repo_url": repo_url,
            "crontab": crontab,
            "analytics_token": analytics_token,
        }
