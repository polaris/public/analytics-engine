from celery_sqlalchemy_scheduler import SessionManager
from dynaconf import settings
from flask_sqlalchemy import SQLAlchemy

session_manager = SessionManager()
engine, Session = session_manager.create_session(
    settings.get("SQLALCHEMY_DATABASE_URI", None)
)
session_manager.prepare_models(engine)
scheduler_db = Session()

db = SQLAlchemy()


def init_app(app):
    db.init_app(app)
