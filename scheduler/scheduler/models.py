import enum
from datetime import datetime

from sqlalchemy_serializer import SerializerMixin

from scheduler.ext.database import db


class Runner(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    description = db.Column(db.Text)
    access_token = db.Column(db.String(64))


class Run(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    runner_id = db.Column(db.Integer, db.ForeignKey("runner.id"), nullable=True)
    planned_for = db.Column(db.DateTime)
    executed_at = db.Column(db.DateTime)
    finished_at = db.Column(db.DateTime)
    result = db.Column(db.String(40))
    log = db.Column(db.Text)


class Job(db.Model, SerializerMixin):

    serialize_rules = ("-logs.job.logs",)

    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer, nullable=True)
    name = db.Column(db.String(64), index=True)
    crontab = db.Column(db.String(64))
    repo_url = db.Column(db.String(255))
    created_at = db.Column(db.DateTime, default=datetime.now())
    result = db.Column(db.String(40), nullable=True)
    logs = db.relationship("JobLog", backref="job", cascade="all,delete")
    result_token = db.Column(db.String(64), index=True)
    analytics_token = db.Column(db.String(64))

    def __repr__(self):
        return f"Job: {self.id} task_id: {self.task_id}"


class JobLogStatusEnum(enum.Enum):
    successful = "successful"
    failure = "failure"


class JobLog(db.Model, SerializerMixin):
    id = db.Column(db.Integer, primary_key=True)
    log = db.Column(db.Text, nullable=True)
    status = db.Column(db.Enum(JobLogStatusEnum))
    created_at = db.Column(db.DateTime, default=datetime.now())
    job_id = db.Column(db.Integer, db.ForeignKey("job.id"), nullable=False)
