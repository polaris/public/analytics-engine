# Analytics Engine
This project is divided into the two main parts the scheduler and the runner

## Scheduler
The scheduler can be found in the scheduler folder.


### Periodic Tasks

Analytics engines are stored as YAML files in the `configuration` directory. 

`scheduler read-configs` reads all existing YAML files and starts or updates analytics engines. Since analytics engine configuration might change over time, a crontab, that runs `scheduler read-configs`, should be registerd.

